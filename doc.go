// Copyright 2012, vendion. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

/*  Filename:    doc.go
 *  Author:      vendion <vendion@vendion.net>
 *  Created:     2012-03-04 22:34:57.241209 -0500 EST
 *  Description: Godoc documentation for Go-nilfsviewer
 */


/*
Go-nilfsviewer does...

Usage:

    Go-nilfsviewer [options] ARGUMENT ...

Arguments:

Options:

*/
package documentation
