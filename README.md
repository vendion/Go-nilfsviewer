
[install go]: http://golang.org/install.html "Install Go"
[the godoc url]: http://localhost:6060/pkg/github.com/vendion@gamil.com/Go-nilfsviewer/ "the Godoc URL"

About Go-nilfsviewer
=============

Go-nilfsviewer is the best program for...

Documentation
=============

Usage
-----

Run Go-nilfsviewer with the command

    Go-nilfsviewer [options]

Prerequisites
-------------

[Install Go][].

Installation
-------------

Use goinstall to install Go-nilfsviewer

    goinstall github.com/vendion@gamil.com/Go-nilfsviewer

General Documentation
---------------------

Use godoc to vew the documentation for Go-nilfsviewer

    godoc github.com/vendion@gamil.com/Go-nilfsviewer

Or alternatively, use a godoc http server

    godoc -http=:6060

and visit [the Godoc URL][]


Author
======

vendion &lt;vendion@vendion.net&gt;

Copyright & License
===================

Copyright (c) 2012, vendion.
All rights reserved.
Use of this source code is governed by a BSD-style license that can be
found in the LICENSE file.
